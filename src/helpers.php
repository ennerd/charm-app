<?php
declare(strict_types=1);

namespace Charm;

use Charm\App\ConfigService;
use Charm\App\Context;

function app(string $service = null): mixed
{
    if (null === $service) {
        return App::instance();
    } else {
        return App::instance()->get($service);
    }
}

function context(): Context
{
    return app()->getContext();
}

function config(): ConfigService
{
    return app()->get('config');
}
