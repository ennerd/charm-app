<?php
declare(strict_types=1);

namespace Charm\App;

use Charm\Hooks;

/**
 * Class returns instances based on a type definition, used for auto wiring.
 */
class InstanceFinderService
{
    private static ?self $_instance = null;
    private Hooks $hooks;

    public static function instance(): self
    {
        if (static::$_instance) {
            return static::$_instance;
        }

        return static::$_instance = new self();
    }

    public function __construct()
    {
        $this->hooks = Hooks::instance();
    }

    public function find(string $type, string $identifier): mixed
    {
        return $this->hooks->dispatchToFirst(__METHOD__, $identifier, $type);
    }
}
