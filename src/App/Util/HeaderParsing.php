<?php
declare(strict_types=1);

namespace Charm\App\Util;

class HeaderParsing
{
    public static function parse(string $headerLine): array
    {
        $parts = array_map('trim', explode(',', $headerLine));

        $result = [];

        foreach ($parts as $part) {
            $components = explode(';', $part);
            $result[] = new self($components);
        }

        usort($result, function (mixed $left, mixed $right) {
            return (int) ((($right->q ?? 1.0) - ($left->q ?? 1.0)) * 1000);
        });

        return $result;
    }

    protected array $components;

    protected function __construct(array $components)
    {
        $this->components = $components;
    }

    public function __toString()
    {
        return $this->components[0];
    }

    public function __get(string $component)
    {
        $l = \strlen($component);
        $component = strtolower($component);
        foreach ($this->components as $componentString) {
            if (strtolower(substr($componentString, 0, $l + 1)) === $component.'=') {
                return $this->component = substr($componentString, $l + 1);
            }
        }

        return $this->component = null;
    }
}
