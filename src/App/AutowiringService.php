<?php
declare(strict_types=1);

namespace Charm\App;

use Psr\Container\ContainerInterface;
use ReflectionFunction;

/**
 * Given a service container this service will generate an array of arguments for a
 * callback. If the service container provides a service named 'instance_finder' that
 * provides a method `find(string $type, $identifier)`, it will try to resolve
 * objects as well.
 *
 * @todo Move this into a separate package `charm/autowiring`
 */
class AutowiringService
{
    protected $configCache = [];

    public static function instance(ContainerInterface $container = null): self
    {
        return new self($container);
    }

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Loads configs by searching for the configuration key in the /config/ folders
     * in the application.
     *
     * @return void
     */
    public function wire(callable $f, array $params, array $extra = [], bool $useServiceContainer = true): array
    {
        if (null === $this->container) {
            $userServiceContainer = false;
        }
        $ref = new ReflectionFunction($f);
        $paramsByOffset = array_values($params);
        $argumentList = [];
        foreach ($ref->getParameters() as $offset => $parameter) {
            /*
             * Must allow values to be passed by value. This is usually not relevant
             */
            if (!$parameter->canBePassedByValue()) {
                throw new Error("Argument '".$parameter->getName()."' requires pass by reference", 1001004);
            }

            /*
             * We don't support variadic yet
             */
            if ($parameter->isVariadic()) {
                break;
            }

            $type = $parameter->getType();
            $typeName = (string) $type;

            if (!\array_key_exists($offset, $params) && (null === $type || \in_array($typeName, ['', 'int', 'float', 'string', 'bool', 'mixed']))) {
                /*
                 * For scalar and variant types, we have to rely on the argument name to infer what
                 * to inject.
                 */

                if (\array_key_exists($parameter->getName(), $params)) {
                    /**
                     * A direct mapping is available.
                     */
                    $value = $params[$parameter->getName()];
                } elseif (\array_key_exists($parameter->getName(), $extra)) {
                    /**
                     * A fallback value is availble.
                     */
                    $value = $extra[$parameter->getName()];
                } elseif ($parameter->isDefaultValueAvailable()) {
                    $value = $parameter->getDefaultValue();
                } elseif ($parameter->allowsNull() || $parameter->isOptional()) {
                    $value = null;
                } else {
                    throw new Error("Unable to autowire argument '".$parameter->getName()."' because no values match that name. Available names: ".implode(', ', array_keys(array_merge($params, $extra))), 1001000);
                }
                switch ($typeName) {
                    case 'string':
                        $value = (string) $value;
                        break;
                    case 'int':
                        $value = (int) $value;
                        break;
                    case 'float':
                        $value = (float) $value;
                        break;
                    case 'bool':
                        $value = (bool) $value;
                        break;
                    case '':
                    default:
                        break;
                }
                $argumentList[] = $value;
            } elseif ('' !== $typeName || \array_key_exists($offset, $params)) {
                /*
                 * For particular types we use the same rules as untyped
                 * arguments to find which value to inject.
                 */
                if (\array_key_exists($offset, $params)) {
                    $value = $params[$offset];
                } elseif (\array_key_exists($parameter->getName(), $params)) {
                    /**
                     * A direct mapping seems to be available.
                     */
                    $value = $params[$parameter->getName()];
                } elseif (\array_key_exists($parameter->getName(), $extra)) {
                    /**
                     * A fallback value could be available.
                     */
                    $value = $extra[$parameter->getName()];
                } elseif ($useServiceContainer && $this->container->has($parameter->getName())) {
                    /**
                     * A service with the same name is available. We test parameter name
                     * first, because ambiguity between parameter name and type hint will be
                     * discovered this way.
                     */
                    $value = $this->container->get($parameter->getName());
                } elseif ($useServiceContainer && $this->container->has($typeName)) {
                    /**
                     * Is there a service matching the type hint?
                     */
                    $value = $this->container->get($typeName);
                } else {
                    throw new Error("Unable to autowire argument '$typeName ".$parameter->getName()."' because no values match that name, and no services are named '$typeName' or '".$parameter->getName()."', and other variables are: ".implode(', ', array_keys(array_merge($params, $extra))), 1001001);
                }

                if (!is_a($value, $typeName) && \in_array(\gettype($value), ['int', 'string', 'double', 'bool'])) {
                    // try the instance_finder service
                    if ($this->container && $this->container->has('instance_finder')) {
                        $loader = $this->container->get('instance_finder');
                        $value = $loader->find($typeName, $value);
                    }

                    if (null === $value) {
                        throw new Error("Resource '$value' was not found", 404);
                    }
                    throw new Error("Unable to autowire argument '$typeName ".$parameter->getName()."' because the resolved value is of the wrong class: '".\get_class($value)."'", 1001002);
                }
                $argumentList[] = $value;
            }
        }

        return $argumentList;
    }
}
