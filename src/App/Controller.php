<?php
declare(strict_types=1);

namespace Charm\App;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

abstract class Controller implements ControllerInterface, ResponseFactoryInterface, StreamFactoryInterface, UriFactoryInterface
{
    protected App $app;
    protected array $options = [
        'base_path' => '', // mounting on the root of the website
    ];

    public function __construct(App $app, array $options = [])
    {
        $this->app = $app;
        $this->options = $options + $this->options;
    }

    /**
     * Invoked immediately after all controllers have been added. Can add
     * event listeners to handle requests and stuff.
     *
     * @return void
     */
    public function init()
    {
    }

    public function createResponse(int $code = 200, string $reasonPhrase = ''): ResponseInterface
    {
        return $this->app->get(ResponseFactoryInterface::class)->createResponse($code, $reasonPhrase);
    }

    public function createStream(string $content = ''): StreamInterface
    {
        return $this->app->get(StreamFactoryInterface::class)->createStream($content);
    }

    public function createStreamFromFile(string $filename, string $mode = 'rb'): StreamInterface
    {
        return $this->app->get(StreamFactoryInterface::class)->createStreamFromFile($filename, $mode);
    }

    public function createStreamFromResource($resource): StreamInterface
    {
        return $this->app->get(StreamFactoryInterface::class)->createStreamFromResource($resource);
    }

    public function createUri(string $uri = ''): UriInterface
    {
        return $this->app->get(UriFactoryInterface::class)->createUri($uri);
    }

    /**
     * Send an API response with a mixed content body that will be transformed.
     */
    protected function respond(mixed $response, array $headers = [], int $status = 200): ResponseInterface
    {
        if (\is_object($response)) {
            if ($response instanceof ResponseInterface) {
                return $response;
            }
        }
        $context = $this->app->get('context');
        $serverRequest = $context->server_request;

        return new ApiResponse($serverRequest, $response, $status, $headers);
    }

    /**
     * Send a HTTP redirect.
     */
    protected function redirect(mixed $target, array $headers = [], int $code = 301): ResponseInterface
    {
        $response = $this->createResponse($code);
        foreach ($headers as $header => $value) {
            $response = $response->withAddedHeader($header, $value);
        }
        $response->withHeader('Location', (string) $target);

        return $response;
    }

    protected function route(string $method, string $path, mixed $handler): self
    {
        $this->app->get('router')->add($method, $this->options['base_path'].$path, $handler);

        return $this;
    }

    protected function GET(string $path, mixed $handler): self
    {
        return $this->route('GET', $path, $handler);
    }

    protected function HEAD(string $path, mixed $handler): self
    {
        return $this->route('HEAD', $path, $handler);
    }

    protected function POST(string $path, mixed $handler): self
    {
        return $this->route('POST', $path, $handler);
    }

    protected function PUT(string $path, mixed $handler): self
    {
        return $this->route('PUT', $path, $handler);
    }

    protected function DELETE(string $path, mixed $handler): self
    {
        return $this->route('DELETE', $path, $handler);
    }

    protected function CONNECT(string $path, mixed $handler): self
    {
        return $this->route('CONNECT', $path, $handler);
    }

    protected function OPTIONS(string $path, mixed $handler): self
    {
        return $this->route('OPTIONS', $path, $handler);
    }

    protected function TRACE(string $path, mixed $handler): self
    {
        return $this->route('TRACE', $path, $handler);
    }

    protected function PATCH(string $path, mixed $handler): self
    {
        return $this->route('PATCH', $path, $handler);
    }
}
