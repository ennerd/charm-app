<?php
declare(strict_types=1);

namespace Charm\App;

use Charm\App;

interface ControllerInterface
{
    public function __construct(App $app);

    /**
     * Called to initialize the controller, after all controllers have been added to the application.
     *
     * @return void
     */
    public function init();
}
