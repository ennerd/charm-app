<?php
declare(strict_types=1);

namespace Charm\App;

use Charm\App;
use Charm\Event\EventEmitterInterface;
use Charm\Event\EventEmitterTrait;
use Closure;
use Composer\Autoload\ClassLoader;

/**
 * @psalm-type ModuleOptions = array{
 *   glob_controllers: bool,
 *   create_autoloader: bool
 * }
 */
final class Module implements EventEmitterInterface
{
    use EventEmitterTrait;

    /**
     * Emitted when the module is unloading.
     */
    public const DETACHING_EVENT = self::class.'::DETACHING_EVENT';

    /**
     * Emitted if detaching was cancelled.
     */
    public const DETACH_CANCELLED_EVENT = self::class.'::DETACH_CANCELLED_EVENT';

    /**
     * Emitted after the module has unloaded.
     */
    public const DETACHED_EVENT = self::class.'::DETACHED_EVENT';

    /**
     * Options that can be configured on the module.
     */
    public const DEFAULT_OPTIONS = [
        /*
         * Should we glob('src/*Controller.php') to find controllers, or are they defined in 'config/controllers.php'?
         */
        'glob_controllers' => true,
        'create_autoloader' => true,
    ];
    protected ?App $app;
    protected string $name;
    protected array $options;
    protected string $namespace;
    protected string $root;

    /**
     * Constructor expects the module namespace and the root directory.
     *
     * @psalm-param ModuleOptions $options
     *
     * @param string $name      Name of the module
     * @param string $namespace Namespace for the autoloader that loads files from `src/`
     * @param string $root      The root directory of the module
     * @param array  $options   Any custom options
     */
    public function __construct(string $name, string $namespace, string $root, array $options = self::DEFAULT_OPTIONS)
    {
        $this->name = $name;
        $this->namespace = $namespace;
        $this->root = $root;
        $this->options = $options + self::DEFAULT_OPTIONS;
    }

    /**
     * The class loader for the module.
     */
    protected function classLoader(string $className): void
    {
        if (str_starts_with($className, $this->namespace)) {
            $relativeName = $this->root.'/src/'.strtr(substr($className, \strlen($this->namespace)), '\\', \DIRECTORY_SEPARATOR).'.php';
            if (file_exists($relativeName)) {
                require $relativeName;
            }
        }
    }

    /**
     * Called when the module is loaded by the application, immediately after being constructed.
     */
    public function attach(App $app): void
    {
        if (isset($this->app) && null !== $this->app) {
            throw new Error('Module is already attached');
        }
        $this->app = $app;
        if (is_dir($this->root.'/src')) {
            spl_autoload_register(Closure::fromCallable([$this, 'classLoader']));
        }
        $this->app->get('finder')->addPath($this->root);
    }

    /**
     * Clean up so that this module can be unloaded.
     *
     * @param bool $force Allow detaching to be prevented by event listeners to self::DETACHING_EVENT?
     */
    public function detach(bool $force = false): void
    {
        if (null === $this->app) {
            throw new Error('Module is not attached');
        }
        $this->emit(self::DETACHING_EVENT, $event = (object) [
            'app' => $this->app,
            'module' => $this,
            'cancelled' => false,
        ], true);
        if ($event->cancelled) {
            $this->emit(self::DETACH_CANCELLED_EVENT, (object) [
                'app' => $this->app,
                'module' => $this,
            ], false);

            return;
        }
        spl_autoload_unregister(Closure::fromCallable([$this, 'classLoader']));
        $this->app->get('finder')->removePath($this->root);
        $this->app = null;
        $this->emit(self::DETACHED_EVENT, (object) [
            'app' => $this->app,
            'module' => $this,
        ], false);
    }

    /**
     * Get the root namespace for this module.
     */
    public function namespace(): string
    {
        return $this->namespace;
    }

    /**
     * Get the root path for this module.
     */
    public function root(): string
    {
        return $this->root;
    }

    public function app(): ?App
    {
        return $this->app;
    }
}
