<?php
declare(strict_types=1);

namespace Charm\App\Commands;

use Charm\Cli\AbstractCommand;

class AppCommand extends AbstractCommand
{
    public function getBaseCommand(): string
    {
        return 'app';
    }

    public function getSummary(): string
    {
        return 'Issue requests to the application router';
    }
}
