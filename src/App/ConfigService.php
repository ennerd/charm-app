<?php
declare(strict_types=1);

namespace Charm\App;

use Charm\Hooks;

/**
 * This service provider provides configurations identified by strings. It
 * does not use a Service Container because it usually is needed before
 * a service container is made available.
 */
class ConfigService
{
    protected $configCache = [];

    public static function instance(): self
    {
        return new self();
    }

    public function integrate()
    {
        $hooks = Hooks::instance();

        /*
         * Integrates `config/db.php` with Charm\DB::instance()
         */
        Hooks::instance()->listen('Charm\DB::getConfig', function (array $config) {
            if (null !== ($newConfig = $this->get('db'))) {
                foreach ($newConfig as $k => $v) {
                    $config[$k] = $v;
                }
            }

            return $config;
        });
    }

    /**
     * Loads configs by searching for the configuration key in the /config/ folders
     * in the application.
     *
     * If the config name is specified with a space, like 'paths files', it will return
     * the key 'files' from the file 'paths.php' in any of the config files, starting
     * with 'config/paths.php', then 'modules/{}/config/paths.php' finally falling back on
     * the default config.
     *
     * @return mixed
     */
    public function get(string $configName)
    {
        $parts = explode(' ', $configName);

        if (isset($this->configCache[$configName])) {
            $result = $this->configCache[$configName];

            $path = $parts[0];
            for ($i = 1; isset($parts[$i]); ++$i) {
                if (isset($parts[$i + 1]) && !\is_array($parts[$i])) {
                    throw new Error("Configuration '$path' is not an array, but we queried for '$configName'.");
                }
                $path .= ' '.$parts[$i];
                if (isset($result[$parts[$i]])) {
                    $result = $result[$parts[$i]];
                } else {
                    throw new Error("Configuration value for '$path' not found when querying for '$configName'.");
                }
            }

            return $result;
        }

        $debugMode = \defined('DEBUG') && (bool) \constant('DEBUG');
        $configs = [];
        foreach (FinderService::instance()->find('config/'.$parts[0].'.php') as $path) {
            $configs[$path] = $got = require $path;
            if ($debugMode) {
                if (1 === $got) {
                    throw new DebugError("Config file '$path' does not return a value");
                }
            }
        }

        /**
         * Sorts configuration files by path length. The intention is that we make sure
         * configuration files from the application is more important than configuration
         * from modules and plugins. Finally, the Charm\App configuration is least important.
         */
        $fallbackConfigRoot = \dirname(__DIR__, 2).'/config';
        uksort($configs, function (string $a, string $b) use ($fallbackConfigRoot): int {
            if (str_starts_with($a, $fallbackConfigRoot)) {
                return 1;
            }
            if (str_starts_with($b, $fallbackConfigRoot)) {
                return -1;
            }

            return \strlen(\dirname($a)) - \strlen(\dirname($b));
        });

        if (\count($configs) > 1) {
            // merging configs require all arrays
            $finalConfig = [];
            foreach ($configs as $c) {
                if (\is_array($c)) {
                    foreach ($c as $k => $v) {
                        if (\is_int($k)) {
                            $finalConfig[] = $v;
                        } elseif (!\array_key_exists($k, $finalConfig)) {
                            $finalConfig[$k] = $v;
                        }
                    }
                } else {
                    throw new Error("Can't merge configurations ($configName) that don't return array");
                    $finalConfig = $c;
                    break;
                }
            }
            $this->configCache[$configName] = $finalConfig;
        } else {
            if (0 === \count($configs)) {
                throw new Error("Config '$configName' not found");
            }
            $config = current($configs);
            $this->configCache[$configName] = $config;
        }

        // recursive call because we want to use the search algo
        return $this->get($configName);
    }
}
