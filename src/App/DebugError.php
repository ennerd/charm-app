<?php
declare(strict_types=1);

namespace Charm\App;

/**
 * Exceptions thrown when in debug mode. Checks that we actually are in debug mode, in case
 * the developer made a mistake.
 */
class DebugError extends Error
{
    public function __construct(string $message = '', mixed $code = 0, Throwable $previous = null, array $extraInfo = [])
    {
        if (!(\defined('DEBUG') && \constant('DEBUG'))) {
            throw new Error('DebugError thrown when not in DEBUG mode');
        }
        parent::__construct($message, $code, $previous, $extraInfo);
    }
}
