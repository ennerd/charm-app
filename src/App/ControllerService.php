<?php
declare(strict_types=1);

namespace Charm\App;

use Charm\App;
use Charm\Controller;
use IteratorAggregate;

class ControllerService implements IteratorAggregate
{
    protected static self $instance;
    protected App $app;
    protected array $controllers = [];
    protected bool $haveInitializedControllers = false;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function init(): void
    {
        $this->loadControllers();
        $this->initializeControllers();
    }

    /**
     * Add a controller to the application. This is only possible during the loading and initializing phase.
     */
    public function add(ControllerInterface $controller): void
    {
        if (App::isDebugMode() && \in_array($controller, $this->controllers)) {
            throw new DebugError("Controller instance is already added to application (class='".$controller::class."')");
        }
        $this->controllers[] = $controller;
        if ($this->haveInitializedControllers) {
            $controller->init();
        }
    }

    public function getIterator(): iterable
    {
        foreach ($this->controllers as $controller) {
            yield $controller;
        }
    }

    protected function loadControllers(): void
    {
        foreach ($this->app->get('config')->get('controllers') as $controllerClass) {
            if (!class_exists($controllerClass)) {
                throw new Error("Controller class '$controllerClass' not found", 500, null, [500, 'Internal Server Error']);
            }
            if (!is_a($controllerClass, ControllerInterface::class, true)) {
                throw new Error("Controllers must implement '".ControllerInterface::class."'");
            }
            $this->add(new $controllerClass($this->app));
        }
    }

    protected function initializeControllers(): void
    {
        $this->haveInitializedControllers = true;
        foreach ($this as $controller) {
            $controller->init();
        }
    }
}
