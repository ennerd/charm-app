<?php
declare(strict_types=1);

namespace Charm\App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Middleware that negotiates Content-Encoding on responses. Currently only supports gzip.
 */
class AcceptEncodingMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $response = $response->withAddedHeader('Vary', 'Accept-Encoding');

        $acceptEncoding = $request->getHeaderLine('Accept-Encoding');
        if (!$acceptEncoding) {
            return $response;
        }

        $encodings = array_map('trim', explode(',', $acceptEncoding));

        $qValue = function (string $value): float {
            $parts = explode(';', $value);
            foreach ($parts as $part) {
                if ('q=' === substr($part, 0, 2)) {
                    return (float) (substr($part, 2));
                }
            }

            return 1.0;
        };

        usort($encodings, function (mixed $left, mixed $right) use ($qValue) {
            return (int) (($qValue($right) - $qValue($left)) * 1000);
        });

        foreach ($encodings as $encoding) {
            if (method_exists($this, $encoding)) {
                $newResponse = $this->$encoding($response);
                if (null !== $newResponse) {
                    return $newResponse;
                }
            }
        }

        return $response;
    }

    public function gzip(ResponseInterface $response): ?ResponseInterface
    {
        if (!\function_exists('gzencode')) {
            return null;
        }

        $body = $response->getBody();
        $size = $body->getSize();

        if (null === $size) {
            return $response;
        }

        $compressedBody = gzencode((string) $body, 9);

        return $response
            ->withHeader('Content-Encoding', 'gzip')
            ->withHeader('Content-Length', (string) \strlen($compressedBody))
            ->withBody($this->createStream($compressedBody));
    }

    protected function createStream(string $content): StreamInterface
    {
        /**
         * @var StreamFactoryInterface
         */
        $factory = \Charm\app(StreamFactoryInterface::class);

        return $factory->createStream($content);
    }
}
