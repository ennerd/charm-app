<?php
declare(strict_types=1);

namespace Charm\App\Middleware;

use Charm\App\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SessionMiddleware implements MiddlewareInterface
{
    private $sessionName;

    public function __construct(string $sessionName = 'charmsession')
    {
        $this->sessionName = $sessionName;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $session = new Session($this->sessionName, $request->getCookieParams());

        $request = $request->withAttribute('session', $session);

        $response = $handler->handle($request);

        return $session->handleResponse($response);
    }
}
