<?php

declare(strict_types=1);

namespace Charm\App;

use Charm\Event\EventEmitterInterface;
use Charm\Event\EventEmitterTrait;
use Charm\Loop\Promise;
use Charm\Util\ComposerPath;
use Closure;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class App implements EventEmitterInterface, RequestHandlerInterface, ContainerInterface
{
    use EventEmitterTrait;

    /**
     * @var self
     */
    private static $instance;

    /**
     * Runtime phases.
     */
    public const PHASE_STARTING = 'starting';
    public const PHASE_LOADING = 'loading';
    public const PHASE_INITIALIZING = 'initializing';
    public const PHASE_READY = 'ready';
    public const PHASE_STOPPING = 'stopping';
    public const PHASE_STOPPED = 'stopped';

    /**
     * Event hooks.
     */
    public const LOADING_DONE_EVENT = 'loading-done';
    public const INITIALIZING_DONE_EVENT = 'initializing-done';
    public const READY_EVENT = 'ready';
    public const STOPPING_EVENT = 'stopping';
    public const STOPPED_EVENT = 'stopped';
    public const CRON_EVENT = 'cron';

    /**
     * @readonly
     */
    public string $root;

    /**
     * @var array<string, mixed>
     */
    private $services = [];

    /**
     * @var ?Context
     */
    private $currentContext;

    public static function instance(): self
    {
        if (self::$instance) {
            return self::$instance;
        }

        return self::$instance = new self();
    }

    public static function isDebugMode(): bool
    {
        return \defined('DEBUG') && \constant('DEBUG');
    }

    public function __construct()
    {
        $root = ComposerPath::get();
        if (null === $root) {
            throw new Error("Can't find application root");
        }
        $this->root = $root;

        $this->_phase = self::PHASE_LOADING;

        $this->loadCoreServices();

        /*
         * The config service integrates with other Charm libraries and needs
         * special treatment.
         */
        $this->get('config')->integrate();

        /*
         * The application file, if it exists, is included first. It should be able to integrate deeply.
         */
        if (is_file($this->root.'/app.php')) {
            /**
             * @psalm-suppress UnresolvableInclude
             */
            require $this->root.'/app.php';
        }

        /*
         * Modules are loaded immediately after, because they can provide services, controllers and configuration
         */
        $this->loadModules();

        $this->loadServices();

        $this->emit(self::LOADING_DONE_EVENT, (object) [], false);

        $this->_phase = self::PHASE_INITIALIZING;

        $this->get('controllers')->init();

        $this->emit(self::INITIALIZING_DONE_EVENT, (object) [], false);

        $this->_phase = self::PHASE_READY;
        $this->emit(self::READY_EVENT, (object) [], false);
        $this->sortMiddlewares();

        $this->setupRoutes();
    }

    /**
     * Setup application level routes.
     *
     * @return void
     */
    public function setupRoutes()
    {
        /**
         * @var Router
         */
        $router = $this->get('router');
        $router->add('GET', '/charm/cron/run', Closure::fromCallable([$this, 'charm_cron_run']));
    }

    /**
     * Run maintenance jobs.
     *
     * @return ResponseInterface
     */
    protected function charm_cron_run(ServerRequestInterface $request)
    {
        $this->emit(self::CRON_EVENT, $event = (object) [
        ], false);

        return new ApiResponse($request, $event);
    }

    public function getContext(): Context
    {
        if (!$this->currentContext) {
            throw new Error("'App::getContext()' called, but no active context exists.");
        }

        return $this->currentContext;
    }

    protected function replaceContext(Context $context): ?Context
    {
        $oldContext = $this->currentContext;
        $this->currentContext = $context;

        return $oldContext;
    }

    protected function createContext(ServerRequestInterface $request): Context
    {
        $context = new Context($this, [
            'server_request' => $request,
        ]);

        return $this->get('hooks')->filter(__METHOD__, $context, $request);
    }

    /**
     * @see Psr\Container\ContainerInterface::get
     *
     * @return mixed
     */
    public function get(string $id)
    {
        if ($this->has($id)) {
            return $this->services[$id]($this, $this);
        } else {
            // @todo Should throw Psr\Container\*ExceptionInterface
            throw new Error("Unknown service tag '$id'");
        }
    }

    /**
     * @see Psr\Container\ContainerInterface::has
     */
    public function has(string $id): bool
    {
        return \array_key_exists($id, $this->services);
    }

    /**
     * Add a service to the service container.
     *
     * @return void
     */
    public function add(string $service, callable $serviceFactory)
    {
        if ($this->has($service)) {
            throw new Error("Service tag '$service' is already added");
        }
        $this->services[$service] = $serviceFactory;
    }

    /**
     * Expects a callable that will receive a request, response and a next callable,
     * similar to expressjs middleware.
     *
     * @param $middleware A middleware handler
     * @param $weight The priority of the middleware. Lower weights process earlier.
     *
     * @return void
     */
    public function use(MiddlewareInterface $middleware, int $weight = 0)
    {
        $this->_middlewareHandlers[$weight][] = $middleware;
    }

    /**
     * @see RequestHandlerInterface::handle
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /**
         * A request handler instance which processes the middleware queue.
         */
        $requestHandler = new class($this, $this->_middlewareHandlers) implements RequestHandlerInterface {
            protected App $app;
            protected array $middlewares;
            protected int $middlewareIndex = 0;
            protected int $middlewareCount;

            public function __construct(App $app, array $middlewares)
            {
                $this->app = $app;
                $this->middlewares = $middlewares;
                $this->middlewareCount = \count($middlewares);
            }

            public function handle(ServerRequestInterface $request): ResponseInterface
            {
                if ($this->middlewareIndex < $this->middlewareCount) {
                    $middleware = $this->middlewares[$this->middlewareIndex++];

                    return $middleware->process($request, $this);
                }

                $handler = $this->app->get('router')->resolve($request->getMethod(), $request->getUri()->getPath());

                if (\is_object($handler) && $handler instanceof RequestHandlerInterface) {
                    return $handler->handle($request);
                }

                if (!\is_callable($handler[0])) {
                    throw new Error("Request handlers must be callable or an instance of '".RequestHandlerInterface::class."'");
                }

                /*
                 * Process arguments for the handler using ReflectionMethod.
                 */
                $handlerFunction = Closure::fromCallable($handler[0]);
                $autowireService = $this->app->get('autowire');

                $argumentList = $autowireService->wire($handlerFunction, $handler[1], [
                    /*
                     * Extra autowired handler arguments
                     */
                    'request' => $request,
                    'serverRequest' => $request,
                    'server_request' => $request,
                ]);

                $result = \call_user_func_array($handlerFunction, $argumentList);

                if ($result instanceof ResponseInterface) {
                    return $result;
                } elseif ($result instanceof RequestHandlerInterface) {
                    return $result->handle($request);
                } else {
                    $receivedWhat = \is_object($result) ? get_class($result) : \gettype($result);
                    throw new Error("Route handler for '".$request->getRequestTarget()."' returned '$receivedWhat', expected ".ResponseInterface::class." or ".RequestHandlerInterface::class);
                }
            }
        };

        /**
         * Create a context object which will hold state information related to the current request.
         */
        $context = $this->createContext($request);

        $this->replaceContext($context);
        $serverRequest = $request->withAttribute('charm.context', $context);

        /*
         * Wrap the ServerRequest object so that the Context object is
         * up to date with the $request->getAttribute() attributes.
         *
         * TODO Create a proper class instead of this anonymous class.
         *
         * @psalm-suppress InvalidArgument
         */
        /*
        $serverRequest = new class($request, $context) extends ServerRequest implements ServerRequestInterface {
            public function __construct(ServerRequestInterface $request, Context $context)
            {
                parent::__construct(
                    $request->getMethod(),
                    $request->getUri(),
                    $request->getHeaders(),
                    $request->getBody(),
                    $request->getProtocolVersion(),
                    $request->getServerParams()
                );

                $this->attributes = $request->getAttributes();

                $this->attributes['__context__'] = $context;
            }

            public function getAttribute($attribute, $default = null)
            {
                if (isset($this->attributes['__context__']->$attribute)) {
                    return $this->attributes['__context__']->$attribute;
                }

                return parent::getAttribute($attribute, $default);
            }

            public function withAttribute($attribute, $value): self
            {
                if (isset($this->attributes['__context__']->$attribute)) {
                    throw new Error('Trying to set attribute which is owned by Context. Use Context to set this attribute.');
                }

                return parent::withAttribute($attribute, $value);
            }

            public function getAttributes(): array
            {
                $attributes = parent::getAttributes();
                $context = $attributes['__context__'];
                unset($attributes['__context__']);
                foreach ($context->getAllVariables() as $key => $value) {
                    $attributes[$key] = $value;
                }

                return $attributes;
            }
        };
        */

        return $requestHandler->handle($serverRequest);
    }

    /**
     * Load Core Service Providers.
     *
     * @return void
     */
    protected function loadCoreServices()
    {
        $coreServices = require \dirname(__DIR__, 2).'/config/core/services.php';
        foreach ($coreServices as $service => $factory) {
            $this->add($service, $factory);
        }
    }

    /**
     * Load Modules.
     *
     * @return void
     */
    protected function loadModules()
    {
        $debugMode = self::isDebugMode();
        $finder = FinderService::instance();
        $paths = [];
        foreach (FinderService::instance()->find('modules/*') as $moduleRoot) {
            $modulePath = $moduleRoot.'/module.php';
            if ($debugMode && !file_exists($modulePath = $moduleRoot.'/module.php')) {
                throw new DebugError("Expected to find file '$modulePath'.");
            }
            /**
             * @psalm-suppress UnresolvableInclude
             */
            $module = require $modulePath;
            if (!\is_object($module) || !($module instanceof Module)) {
                throw new Error("File '$modulePath' must return an instance of ".Module::class);
            }
            $this->_modules[] = $module;
            $paths[] = $module->root();
        }
        foreach ($this->_modules as $module) {
            $module->attach($this);
        }
    }

    /**
     * Load Service Providers.
     */
    protected function loadServices(): void
    {
        foreach ($this->get('config')->get('services') as $service => $factory) {
            $this->add($service, $factory);
        }
    }

    private function sortMiddlewares(): void
    {
        $middlewares = [];
        ksort($this->_middlewareHandlers, \SORT_NUMERIC);
        foreach ($this->_middlewareHandlers as $k => $t) {
            foreach ($t as $middleware) {
                $middlewares[] = $middleware;
            }
        }
        $this->_middlewareHandlers = $middlewares;
    }

    protected array $_modules = [];
    protected array $_middlewareHandlers = [];
    protected string $_phase = self::PHASE_STARTING;
}
