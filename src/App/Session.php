<?php
declare(strict_types=1);

namespace Charm\App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Session
{
    protected $request;
    protected $started = false;
    protected $sessionName;
    protected $session;
    protected $writes = [];

    public function __construct(string $sessionName)
    {
        $this->sessionName = $sessionName;
    }

    public function __get(string $key)
    {
        $this->start();
    }

    public function __set(string $key, $value)
    {
        $this->writes[$key] = $name;
    }

    protected function start(): void
    {
        if ($this->started) {
            return;
        }
        session_name($this->sessionName);
        $cookie = $this->request->getCookieParams();
        var_dump($cookie);
        exit();
        session_start();
    }

    public function handlaeRequest(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    public function handleResponse(ResponseInterface $response): ResponseInterface
    {
        return $response;
    }
}
