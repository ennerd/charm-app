<?php
declare(strict_types=1);

namespace Charm\App;

use Charm\Util\ComposerPath;
use Generator;

/**
 * Finds files in the various root folders that a project can consist of.
 * Searches in /, /modules/ * / and /vendor/charm/app/.
 */
class FinderService
{
    protected static ?self $_instance = null;
    protected array $pathCache = [];
    protected array $paths = [];
    protected string $finalPath;

    public static function instance(): self
    {
        if (static::$_instance) {
            return static::$_instance;
        }

        $path = ComposerPath::get();
        if (null === $path) {
            throw new Error('Requires composer');
        }

        return static::$_instance = new self($path);
    }

    public function __construct(string $root)
    {
        $this->paths = [
            $root,                  // path to application root
        ];
        $this->finalPath = \dirname(__DIR__, 2);   // path where the charm/app library is installed
    }

    /**
     * Add a path to the finder.
     */
    public function addPath(string $path): void
    {
        // the cache becomes void
        $this->pathCache = [];
        $this->paths[] = rtrim($path, '/');
    }

    /**
     * Remove a path from the finder.
     */
    public function removePath(string $path): void
    {
        $this->pathCache = [];
        $this->paths[] = array_filter($this->paths, function (mixed $value) use ($path): bool {
            return $path !== $value;
        });
    }

    /**
     * @return Generator
     */
    public function find(string $pattern)
    {
        $pattern = ltrim($pattern, '/');

        if (isset($this->pathCache[$pattern])) {
            yield from $this->pathCache[$pattern];

            return;
        }
        $paths = $this->paths;
        $paths[] = $this->finalPath;
        // this for(;;) loop allows new paths to be added while finding
        for ($i = 0; isset($paths[$i]); ++$i) {
            $glob = $paths[$i];
            foreach (glob($glob.'/'.$pattern) as $candidate) {
                $this->pathCache[$pattern][] = $candidate;
                yield $candidate;
            }
        }
    }
}
