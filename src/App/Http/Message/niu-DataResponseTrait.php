<?php
declare(strict_types=1);

namespace Charm\App\Http\Message;

use Charm\App\Util\HeaderParsing;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

trait DataResponseTrait
{
    /**
     * Return structured data which can be serialized to JSON, XML or other negotiatable
     * formats.
     *
     * @return scalar|array|object Return data
     */
    abstract protected function getApiResponse(ServerRequestInterface $request);

    final public function getResponse(ServerRequestInterface $request): ResponseInterface
    {
        /** @var ResponseFactoryInterface */
        $responseFactory = \Charm\app(ResponseFactoryInterface::class);

        /** @var StreamFactoryInterface */
        $streamFactory = \Charm\app(StreamFactoryInterface::class);

        try {
            $data = $this->getApiResponse($request);
            $serialized = $this->serializeAccordingToRequest($data, $request);

            return $responseFactory
                ->createResponse(200)
                ->withBody($streamFactory->createStream($serialized->body))
                ->withAddedHeader('Content-Type', $serialized->contentType);
        } catch (\Charm\Exception $e) {
        }
    }

    private function serializeAccordingToRequest(mixed $data, ServerRequestInterface $request): object
    {
        $accept = $request->getHeaderLine('Accept');
        foreach (HeaderParsing::parse($accept) as $part) {
            switch ((string) $part) {
                default:
                case 'application/json':
                    return (object) [
                        'contentType' => 'application/json',
                        'body' => json_encode($data),
                    ];
                /*
                default:
                case 'text/html':
                    return (object) [
                        'contentType' => 'text/html',
                        'body' => self::htmlFormatter($data, $request),
                    ];
                */
            }
        }
    }

    private static function htmlFormatter(mixed $data, ServerRequestInterface $request)
    {
        $title = htmlspecialchars($request->getMethod().' '.$request->getRequestTarget());
        $head = <<<EOT
            <!DOCTYPE html>
            <html>
                <head>
                    <title>$title</title>
                    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.1.0/styles/default.min.css">
                    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.1.0/highlight.min.js"></script>
                </head>
                <body><pre><code>
            EOT;

        $foot = <<<EOT
                </code></pre>
                <style scoped>
                    body, html {
                        margin: 0;
                        padding: 0;
                        min-height: 100vh;
                    }

                    pre {
                        margin: 0;
                        padding: 0;
                        width: 100%;
                        min-height: 100vh;
                    }
                </style>
                <script>hljs.highlightAll();</script>
                </body>
            </html>
            EOT;

        return $head.json_encode($data, \JSON_PRETTY_PRINT).$foot;
    }
}
/*
} catch (\Charm\Exception $e) {
    return new ApiResponse($request, $e, $e->getHttpCode(), [], $e->getReasonPhrase());
} catch (\Throwable $e) {
    throw $e;
    $result = [
        'message' => $e->getMessage(),
        'code' => $e->getCode(),
        'http' => [
            'code' => 500,
            'reasonPhrase' => 'Internal Server Error',
        ],
    ];

    if (defined('DEBUG') && constant('DEBUG')) {
        $result['DEBUG_IS_ENABLED'] = [
            'type' => $e::class,
            'line' => $e->getLine(),
            'file' => $e->getFile(),
            'trace' => $e->getTrace(),
        ];
    }

    return new ApiResponse($request, $result, 500);
}
*/
