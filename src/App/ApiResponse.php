<?php
declare(strict_types=1);

namespace Charm\App;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Suppressing everything because psalm apparently gets everything wrong here.
 *
 * @psalm-suppress all
 */
class ApiResponse extends Response
{
    /**
     * @param array<string, array<string>|string> $headers
     * @param string                              $reason
     */
    public function __construct(ServerRequestInterface $serverRequest, mixed $value, int $status = 200, array $headers = [], string $reason = null)
    {
        //__construct(int $status = 200, array $headers = [], $body = null, string $version = '1.1', string $reason = null)
        /*
         * @psalm-suppress all
         */
        $headers['Content-Type'] = 'application/json';
        static::init($status, $headers, json_encode($value, \JSON_PRETTY_PRINT), '1.1', $reason);
    }
}
