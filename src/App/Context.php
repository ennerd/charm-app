<?php
declare(strict_types=1);

namespace Charm\App;

use JsonSerializable;

final class Context implements JsonSerializable
{
    /**
     * Hook to add properties and customize the Context object.
     */
    public const SETUP_HOOK = self::class.'::SETUP_HOOK';
    private App $_app;
    private bool $_initialized = false;

    /**
     * Context object is an object with a strict set of configurable properties that is available
     * whenever a request is being processed. In environments where multiple requests can be processed
     * simultaneously (Swoole, ReactPHP, amp etc), care must be taken to ensure we're working with the
     * correct context object instance.
     */
    public function __construct(App $app, array $initialPropertyValues = [])
    {
        $this->_app = $app;

        foreach ($initialPropertyValues as $propertyName => $propertyValue) {
            $this->propertyValues[$propertyName] = $propertyValue;
        }

        /**
         * Hook to allow declaring additional properties on the context object.
         *
         * @var \Charm\Hooks
         */
        $hooks = $app->get('hooks');
        $hooks->dispatch(self::SETUP_HOOK, $this);
        $this->_initialized = true;
    }

    private array $declaredProperties = [];
    private array $propertyValues = [];

    public function declareProperty(string $propertyName, string $typeName): void
    {
        if ($this->_initialized) {
            throw new Error("Can't declare properties on Context after initialization");

            return;
        }
        if ($this->hasProperty($propertyName)) {
            throw new Error('Declaring a Context Property twice is illegal');
        }
        $this->declaredProperties[$propertyName] = $typeName;
    }

    private function hasProperty(string $propertyName): bool
    {
        return \array_key_exists($propertyName, $this->declaredProperties);
    }

    private function assertHasProperty(string $propertyName): void
    {
        if (!$this->hasProperty($propertyName)) {
            throw new Error("Property '$propertyName' is not declared using Context::declareProperty()");
        }
    }

    private function assertValidProperty(string $propertyName, mixed $value): void
    {
        $this->assertHasProperty($propertyName);

        $declaredType = $this->declaredProperties[$propertyName];

        $nullable = '?' === $declaredType[0] || 'mixed' === $declaredType;

        if (null === $value) {
            if (!$nullable) {
                throw new Error("Property '$propertyName' is not declared as 'mixed' and the type name does not begin with '?' so null values is not allowed.");
            }

            return;
        }

        $typeName = ltrim($declaredType, '?');
        $actualType = \gettype($value);

        if ($typeName === $actualType) {
            return;
        }

        if ('number' === $typeName && ('integer' === $actualType || 'double' === $actualType || 'string' === $actualType)) {
            return;
        }

        if ('object' === $actualType) {
            $actualType = \get_class($value);
        }

        if (is_a($value, $typeName)) {
            return;
        }

        throw new Error("Property '$propertyName' must be of type '$typeName', got '$actualType'");
    }

    public function __set(string $name, mixed $value)
    {
        $this->assertValidProperty($name, $value);
        $this->propertyValues[$name] = $value;
    }

    public function __get(string $name)
    {
        if (\array_key_exists($name, $this->propertyValues)) {
            return $this->propertyValues[$name];
        }
        $this->assertHasProperty($name);

        return null;
    }

    public function __isset(string $name)
    {
        return $this->hasProperty($name);
    }

    public function __unset(string $name)
    {
        throw new Error("Can't unset properties on the context object. Consider setting the value to 'null'.");
    }

    public function getAllVariables(): array
    {
        $result = [];
        foreach ($this->declaredProperties as $key => $typeName) {
            $result[$key] = $this->$key;
        }

        return $result;
    }

    public function jsonSerialize()
    {
        return $this->getAllVariables();
    }

    public function __debugInfo()
    {
        return json_decode(json_encode($this), true);
    }
}
