<?php
declare(strict_types=1);

namespace Charm;

/*
 * TODO Consider adding this class aliases elsewhere
 */
class_alias(App\Controller::class, Controller::class);
