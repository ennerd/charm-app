<?php
declare(strict_types=1);

use Charm\App;
use Charm\Router;
use Psr\Log\LoggerInterface;
use Charm\App\ControllerService;
use Monolog\Handler\StreamHandler;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;

/**
 * This works because services.php is loaded from inside the App class.
 */
$app = $this;

/*
 * Services added to the app factory.
 */
return [
    'app' => function () use ($app) {
        return $app;
    },
    /*
     * This service is created for convenience
     */
    'context' => function (ContainerInterface $container, App $app) {
        return $app->getContext();
    },
    'hooks' => [Charm\Hooks::class, 'instance'],
    'autowire' => [Charm\App\AutowiringService::class, 'instance'],
    'instance_finder' => [Charm\App\InstanceFinderService::class, 'instance'],
    'finder' => [Charm\App\FinderService::class, 'instance'],
    'config' => [Charm\App\ConfigService::class, 'instance'],
    'router' => function (ContainerInterface $container) {
        static $service;
        if ($service) {
            return $service;
        }
        $general = $container->get('config')->get('general');

        return $service = new Router($general['base_url']);
    },
    'db' => [Charm\DB::class, 'instance'],
    'controllers' => function (ContainerInterface $container, App $app) {
        static $service;
        if ($service) {
            return $service;
        }

        return $service = new ControllerService($app);
    },
    'log' => function (ContainerInterface $container, App $app) {
        static $logger;
        if ($logger) {
            return $logger;
        }
        $logger = new \Monolog\Logger('App');
        $logger->pushProcessor(new PsrLogMessageProcessor(null, true));
        $logger->pushHandler(new StreamHandler(STDERR));
        return $logger;
    },
    LoggerInterface::class => function(ContainerInterface $container) {
        return $container->get('log');
    },
    Psr17Factory::class => function (ContainerInterface $app) {
        static $service;
        if ($service) {
            return $service;
        }

        return $service = new Psr17Factory();
    },
    RequestFactoryInterface::class => function (ContainerInterface $app) {
        return $app->get(Psr17Factory::class);
    },
    ResponseFactoryInterface::class => function (ContainerInterface $app) {
        return $app->get(Psr17Factory::class);
    },
    ServerRequestFactoryInterface::class => function (ContainerInterface $app) {
        return $app->get(Psr17Factory::class);
    },
    StreamFactoryInterface::class => function (ContainerInterface $app) {
        return $app->get(Psr17Factory::class);
    },
    UriFactoryInterface::class => function (ContainerInterface $app) {
        return $app->get(Psr17Factory::class);
    },
];
