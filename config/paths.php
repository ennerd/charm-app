<?php
declare(strict_types=1);

use Charm\Util\ComposerPath;

$root = Charm\app()->root;

return [
    'root' => ComposerPath::get(),
    'files' => $root.'/files',
    'var' => $root.'/var',
    'temp' => sys_get_temp_dir(),
];
