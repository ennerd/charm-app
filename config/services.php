<?php
declare(strict_types=1);

/**
 * Return associative array with factories:.
 *
 *  [
 *      'serviceTag' => function() {},
 *  ];
 */

return [];
